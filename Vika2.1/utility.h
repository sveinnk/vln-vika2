#ifndef UTILITY_H
#define UTILITY_H
#include <string>

using namespace std;

string toUppercase(string input);

string toLowercase(string input);

#endif // UTILITY_H
