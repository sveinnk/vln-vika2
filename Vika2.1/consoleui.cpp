#include "consoleui.h"
#include <iomanip>
#include <ctime>
#include <vector>
#include <cctype>

using namespace std;

ConsoleUI::ConsoleUI()
{
    personService = PersonService();
}

void ConsoleUI::start() {
    bool checker = true;
    DisplaySettings current;
    string inp;
    welcomeLine();
    seeCommands();

    while(checker){

        cout <<"Insert command: \n";
        cout << "> ";
        cin >> inp;
        inp = toUppercase(inp);
        cout << endl;


        if(inp == "ADD"){
            char c = addChoice();
            if(toupper(c) == 'S'){
                addPerson();
            }else if(toupper(c) == 'C'){
                addComputer();
            }else if(toupper(c) == 'R'){
                addRelations(current);
            }

        }else if(inp == "DISPLAY"){
            char c = displayChoice();
            if(c == 'S'){
                vector<Person> personlist = personService.getScientists(current);
                displayPerson(personlist);
            }else if(c == 'C'){
                vector<Computer> computerlist = personService.getComputers(current);
                displayComputer(computerlist);
            }else if(c == 'R'){
                if(current.relationsSet){
                    vector<PRelations> relationlist = personService.getPRelations();
                    displayPRelations(relationlist);
                 }else{
                    vector<CRelations> relationlist = personService.getCRelations();
                    displayCRelations(relationlist);
                }
            }

        }else if(inp == "SEARCH"){
            char sCommand;
            string sInp;

            if(searchChoice()){
                searchPerson(sCommand, sInp);
                displayPerson(personService.searchP(sCommand, sInp, current));
            }else{
                searchComputer(sCommand, sInp);
                displayComputer(personService.searchC(sCommand, sInp, current));
            }

        }else if(inp == "DELETE"){
            char inp = deleteChoice();

            if(inp == 'S'){
                deletePerson(current);
            }else if(inp == 'C' ){
                deleteComputer(current);
            }else if(inp == 'R'){
                deleteRelations(current);
            }

        }else if(inp == "QUIT"){
            personService.closeDatabase();
            checker = false;

        }else if(inp == "SETTINGS"){
            displaysettings(current);

        }else if(inp == "COMMANDS"){
            seeCommands();

        }else{
            cout << "*Invalid input*\n\n";
        }
    }
}

char ConsoleUI::addChoice()
{
    char choice;
    bool checker = true;
    char rAns;

    cout << "What do you want to add?\n";
    cout << "- Scientist (S)\n" << "- Computer (C)\n" << "- Relations (R)\n";

    while(checker){
        cin >> rAns;
        choice = toupper(rAns);
        checker = check3options(choice, 'S', 'C', 'R');
    }
    cout << endl;

    return rAns;
}

void ConsoleUI::addPerson()
{
    Person p;
    bool checker = true;

    time_t t = time(NULL);
    tm* timePtr = localtime(&t);

    cout << "First name: "; cin >> p.firstName;
    cout << "Last name: "; cin >> p.lastName;
    while(checker){
        cout << "Gender (M/F): "; cin >> p.gender;
        p.gender = toUppercase(p.gender);
        if(p.gender == "M" || p.gender == "F"){
            checker = false;
        }
    }
    checker = true;

    while(checker){
        cout << "Year of birth: "; cin >> p.yearOfBirth;
        if(p.yearOfBirth <= timePtr->tm_year + 1900){
            checker = false;
        }else{
            cout << "*Invalid input*\n";
        }
    }
    checker =true;


    while(checker){
        cout << "Year of death: ";
        cin >> p.yearOfDeath;
        if(p.yearOfDeath >= p.yearOfBirth){
            checker = false;
        }else{
            cout << "*Invalid input*\n";
        }
    }
    cout << endl;

    p.firstName = toLowercase(p.firstName);
    p.lastName = toLowercase(p.lastName);
    p.firstName[0] = toupper(p.firstName[0]);
    p.lastName[0] = toupper(p.lastName[0]);

    personService.addPerson(p);
}

void ConsoleUI::addComputer()
{
    bool checker = true;
    char builtChar;
    Computer c;

    time_t t = time(NULL);
    tm* timePtr = localtime(&t);

    cout << "Name: "; cin >> c.name;

    while(checker){
        cout << "Year of discovery: ";
        cin >> c.discovery;
        if(c.discovery <= timePtr->tm_year + 1900){
            checker = false;
        }else{
            cout << "*Invalid input*\n";
        }
    }
    checker =true;

    cout << "Computer type: "; cin >> c.type;
    cout << "Was it built? (Y/N): ";
    while(checker){
        cin >> builtChar;
        builtChar = toupper(builtChar);
        checker = check2options(builtChar, 'Y', 'N');
    }
    c.built = builtChar;

    cout << endl;

    c.name[0] = toupper(c.name[0]);
    c.type[0] = toupper(c.type[0]);

    personService.addComputer(c);
}

void ConsoleUI::addRelations(DisplaySettings current)
{
    int pID, cID;

    vector<Person> personlist = personService.getScientists(current);
    displayPerson(personlist);
    cout << endl << endl << endl;
    vector<Computer> computerlist = personService.getComputers(current);
    displayComputer(computerlist);

    cout << "Insert Scientist ID: "; cin >> pID;
    cout << endl;
    cout << "Insert Computer ID: "; cin >> cID;
    cout << endl;

    personService.addRelations(pID, cID);
}

char ConsoleUI::displayChoice()
{
    bool checker = true;
    char qAns;

    cout << "Display Scientists (S), Computers (C) or Relations (R)?\n";

    while(checker){
        cout << "> ";
        cin >> qAns;
        qAns = toupper(qAns);
        checker = check3options(qAns, 'S', 'C', 'R');
    }

    return qAns;
}

char ConsoleUI::deleteChoice()
{
    bool checker = true;
    char qAns;

    cout << "Delete Scientists (S), Computers (C) or Relations (R)?\n";
    cout << "> ";

    while(checker){
        cin >> qAns;
        qAns = toupper(qAns);
        checker = check3options(qAns, 'S', 'C', 'R');
    }

    return qAns;
}

bool ConsoleUI::searchChoice()
{
    bool checker = true;
    bool rAns = false;
    char sChoice;

    cout << "Search for a Scientist (S) or a Computer (C)?\n";

    while(checker){
        cout << "> ";
        cin >> sChoice;
        sChoice = toupper(sChoice);
        checker = check2options(sChoice, 'S', 'C');
    }
    if(sChoice == 'S'){
        rAns = true;
    }
    return rAns;
}

void ConsoleUI::searchPerson(char &sCommand, string &sInp)
{
    bool checker = true;

    cout << "Search by:\n";
    cout << "- ID (I)\n" << "- First Name (F)\n" << "- Last Name (L)\n" << "- Gender (G)\n" << "- Year Of Birth (B)\n" << "- Year Of Death (D)\n";

    while(checker){
        cout <<"> ";
        cin >> sCommand;
        sCommand = toupper(sCommand);
        checker = check6options(sCommand, 'I','F', 'L', 'G', 'B', 'D');
    }

    switch(sCommand){
        case 'I':
            cout << "Enter an ID to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'F':
            cout << "Enter a first name to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'L':
            cout << "Enter a last name to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'G':
            cout << "Enter a gender to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'B':
            cout << "Enter a date to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'D':
            cout << "Enter a date to search for: ";
            cin >> sInp;
            checker = false;
            break;
        default:
            break;
        }

    cout << endl;
}

void ConsoleUI::searchComputer(char &sCommand, string &sInp)
{
    bool checker = true;

    cout << "Search by:\n";
    cout << "- ID (I)\n" <<"- Name (N)\n" << "- Discovery (D)\n" << "- Type (T)\n" << "- Built (B)\n";

    while(checker){
        cout <<"> ";
        cin >> sCommand;
        sCommand = toupper(sCommand);
        checker = check5options(sCommand, 'I', 'N', 'D', 'T', 'B');
    }

    switch(sCommand){
        case 'I':
            cout << "Enter an ID to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'N':
            cout << "Enter a name to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'D':
            cout << "Enter a year to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'G':
            cout << "Enter a type to search for: ";
            cin >> sInp;
            checker = false;
            break;
        case 'B':
            cout << "Enter a creation status to search for: ";
            cin >> sInp;
            checker = false;
            break;
        default:
            break;
        }

    cout << endl;
}

void ConsoleUI::deletePerson(DisplaySettings current)
{
    vector<Person> personlist = personService.getScientists(current);
    displayPerson(personlist);

    Person p = Person();
    cout << "Delete Scientist with ID: "; cin >> p.personID;
    personService.deletePerson(p);
    cout << endl;
}
void ConsoleUI::deleteComputer(DisplaySettings current)
{
    vector<Computer> computerlist = personService.getComputers(current);
    displayComputer(computerlist);

    Computer c = Computer();
    cout << "Delete Computer with ID: "; cin >> c.computerID;
    personService.deleteComputer(c);
    cout << endl;
}

void ConsoleUI::deleteRelations(DisplaySettings current)
{
    int sID, cID;

    if(current.relationsSet){
        vector<PRelations> relationlist = personService.getPRelations();
        displayPRelations(relationlist);
        cout << "Insert Scientist ID: "; cin >> sID;
        cout << endl;
        cout << "Insert Computer ID: "; cin >> cID;
     }else{
        vector<CRelations> relationlist = personService.getCRelations();
        displayCRelations(relationlist);
        cout << "Insert Computer ID: "; cin >> cID;
        cout << endl;
        cout << "Insert Scientist ID: "; cin >> sID;
    }

    personService.deleteRelations(sID, cID);

}

void ConsoleUI::displayPerson(vector<Person> personlist)
{
    cout << setw(5) << "ID" << setw(15) << "First Name" << setw(15) << "Last Name" << setw(12) << "Gender";
    cout << setw(20) << "Year Of Birth" << setw(20) << "Year Of Death\n";
    lines();
    for(unsigned int i = 0; i < personlist.size(); i++){
        cout << personlist[i];
    }
    lines();
}

void ConsoleUI::displayComputer(vector<Computer> computerlist)
{
    cout << setw(5) << "ID" << setw(35) << "Name" << setw(20) << "Year of Discovery" << setw(15) << "Type" << setw(10) << "Built\n";
    lines();
    for(unsigned int i = 0; i < computerlist.size(); i++){
        cout << computerlist[i];
    }
    lines();
}

void ConsoleUI::displayPRelations(vector<PRelations> relationlist)
{
    cout << setw(15) << "Scientist ID" << setw(15) << "First Name" << setw(15) << "Last Name" << setw(15) << "Computer ID\n";
    lines();
    for(unsigned int i = 0; i < relationlist.size(); i++){
        cout << relationlist[i];
    }
    lines();
}

void ConsoleUI::displayCRelations(vector<CRelations> relationlist)
{
    cout << setw(15) << "Computer ID" << setw(35) << "Name" << setw(20) << "Scientist ID\n";
    lines();
    for(unsigned int i = 0; i < relationlist.size(); i++){
        cout << relationlist[i];
    }
    lines();
}

bool ConsoleUI::check2options(char input, char option1, char option2)
{
    bool checker = true;

    if(input == option1 || input == option2){
        checker = false;
    }else{
        cout << "*Invalid input*" << endl;
    }

    return checker;
}

bool ConsoleUI::check3options(char input, char option1, char option2, char option3)
{
    bool checker = true;

    if(input == option1 || input == option2 || input == option3){
        checker = false;
    }else{
        cout << "*Invalid input*" << endl;
    }

    return checker;
}

bool ConsoleUI::check4options(char input, char option1, char option2, char option3, char option4)
{
    bool checker = true;

    if(input == option1 || input == option2 || input == option3 || input == option4){
        checker = false;
    }else{
        cout << "*Invalid input*" << endl;
    }

    return checker;
}

bool ConsoleUI::check5options(char input, char option1, char option2, char option3, char option4, char option5)
{
    bool checker = true;

    if(input == option1 || input == option2 || input == option3 || input == option4 || input == option5){
        checker = false;
    }else{
        cout << "*Invalid input*" << endl;
    }

    return checker;
}

bool ConsoleUI::check6options(char input, char option1, char option2, char option3, char option4, char option5, char option6)
{
    bool checker = true;

    if(input == option1 || input == option2 || input == option3 || input == option4 || input == option5 || input == option6){
        checker = false;
    }else{
        cout << "*Invalid input*" << endl;
    }

    return checker;
}

void ConsoleUI::lines()
{
    for(int k = 0; k < 90 ; k++){
        cout << "-";
    }
    cout << endl;
}

void ConsoleUI::displaysettings(DisplaySettings& current)
{
    bool checker = true;
    char ans1, ans2;

    cout << "CURRENT DISPLAY SETTINGS: \n\n";
    cout << "- Scientists sorted by:\n" << "\t" << current.sortPBy << " - " << current.sortPType << endl;
    cout << "- Computers sorted by:\n" << "\t" << current.sortCBy << " - " << current.sortCType << endl << endl;
    cout << "- Relations based on: " << current.relationsString << endl << endl;
    cout << "Do you want to change the settings? (Y/N)\n";

    while(checker){
        cout << "> ";
        cin >> ans1;
        ans1 = toupper(ans1);
        checker = check2options(ans1, 'Y', 'N');
    }
    checker = true;
    cout << endl;

    if(ans1 == 'Y'){
        cout << "Change: Scientist sorting (S), Computer sorting (C) or Relations settings (R)?\n";
        cout << "> ";

        while(checker){
            cin >> ans2;
            ans2 = toupper(ans2);
            checker = check3options(ans2, 'S', 'C', 'R');
        }
        checker = true;

        if(ans2 == 'S'){
            scientistSorting(current);
        }else if(ans2 == 'C'){
            computerSorting(current);
        }else{
            relationsSorting(current);
        }
    }
}

void ConsoleUI::scientistSorting(DisplaySettings& current)
{
    bool checker = true;
    char dCommand;
    char dGet;
    string rCommand;

    cout << endl <<"DISPLAY BASED ON:\n\n";
    cout << "- ID (I)" << "- First Name (F)\n" << "- Last Name (L)\n" << "- Gender (G)\n" << "- Year Of Birth (B)\n" << "- Year Of Death (D)\n";

    while(checker){
        cout << "> ";
        cin >> dCommand;
        dCommand = toupper(dCommand);
        checker = check6options(dCommand, 'I','F', 'L', 'G', 'B', 'D');
    }
    checker = true;
    cout << endl;

    current.sortPByChar = dCommand;


    if(dCommand == 'F' || dCommand == 'L'){
        cout << "Alphabetical (A) or Reverse Alphabetical (R) order?\n";
        while(checker){
            cout << "> ";
            cin >> dGet;
            dGet = toupper(dGet);
            checker = check2options(dGet, 'A', 'R');
        }
        checker = true;

        if(dGet == 'A'){
            current.sortPTypeBool = true;
        }else{
            current.sortPTypeBool = false;
        }

    }else if(dCommand == 'G'){
        cout << "Males first (M) or Females first (F)?\n";
        while(checker){
            cout << "> ";
            cin >> dGet;
            dGet = toupper(dGet);
            checker = check2options(dGet, 'M', 'F');
        }
        checker = true;

        if(dGet == 'M'){
            current.sortPTypeBool = true;
        }else{
            current.sortPTypeBool = false;
        }

    }else{
        cout << "Ascending (A) or Descending (D) order?\n";
        while(checker){
            cout << "> ";
            cin >> dGet;
            dGet = toupper(dGet);
            checker = check2options(dGet, 'A', 'D');
        }
        if(dGet == 'A'){
            current.sortPTypeBool = true;
        }else{
            current.sortPTypeBool = false;
        }
        checker = true;
    }

    cout << endl;

    current.setPSort();
}

void ConsoleUI::computerSorting(DisplaySettings& current)
{
    bool checker = true;
    char dCommand;
    char dGet;
    string rCommand;

    cout << endl <<"DISPLAY BASE ON:\n\n";
    cout << "- ID (I)\n" << "- Name (N)\n" << "- Year of Discovery (D)\n" << "- Computer Type (T)\n" << "- Built (B)\n";

    while(checker){
        cout << "> ";
        cin >> dCommand;
        dCommand = toupper(dCommand);
        checker = check5options(dCommand, 'I', 'N', 'D', 'T', 'B');
    }
    checker = true;
    cout << endl;

    current.sortCByChar = dCommand;

    if(dCommand == 'N' || dCommand == 'T'){
        cout << "Alphabetical (A) or Reverse Alphabetical (R) order?\n";
        while(checker){
            cout << "> ";
            cin >> dGet;
            dGet = toupper(dGet);
            checker = check2options(dGet, 'A', 'R');
        }
        checker = true;

        if(dGet == 'A'){
            current.sortCTypeBool = true;
        }else{
            current.sortCTypeBool = false;
        }

    }else if(dCommand == 'D' || dCommand == 'I'){
        cout << "Ascending (A) or Descending (D)?\n";
        while(checker){
            cout << "> ";
            cin >> dGet;
            dGet = toupper(dGet);
            checker = check2options(dGet, 'A', 'D');
        }
        checker = true;

        if(dGet == 'A'){
            current.sortCTypeBool = true;
        }else{
            current.sortCTypeBool = false;
        }

    }else{
        cout << "Built first (B) or Not built first (N) order?\n";
        while(checker){
            cout << "> ";
            cin >> dGet;
            dGet = toupper(dGet);
            checker = check2options(dGet, 'B', 'N');
        }
        checker = true;

        if(dGet == 'B'){
            current.sortCTypeBool = true;
        }else{
            current.sortCTypeBool = false;
        }
    }

    cout << endl;

    current.setCSort();
}

void ConsoleUI::relationsSorting(DisplaySettings& current)
{
    bool checker = true;
    char inp;

    cout << "Set relations based on:\n";
    cout << "- Scientists (S)\n" << "- Computers (C)";

    while(checker){
        cout << endl << "> ";
        cin >> inp;
        inp = toupper(inp);
        checker = check2options(inp, 'S', 'C');
    }
    cout << endl;

    if(inp == 'S'){
        current.relationsSet = true;
    }else{
        current.relationsSet = false;
    }

    current.setRSort();
}


void ConsoleUI::seeCommands()
{
    cout << "The commands are: \n\n" << "- add\n" << "- delete\n" << "- display\n" << "- search\n" << "- settings\n" << "- commands\n" << "- quit\n\n";
}

void ConsoleUI::welcomeLine(){
    cout << setw(24) << "";
    for(int k = 0; k < 25 ; k++){
        cout << "*";
    }
    cout << endl;
    cout << setw(25)<<" ~~~~~~~~~~~~~~~~~~~~~~ " << "Welcome to the Database" << " ~~~~~~~~~~~~~~~~~~~~~~ " << endl ;
    cout << setw(24) << "";
    for(int k = 0; k < 25 ; k++){
        cout << "*";
    }
    cout << endl;
}
