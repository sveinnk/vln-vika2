#include "personservice.h"


PersonService::PersonService()
{
    personRepo = PersonRepository();
    computerRepo = ComputerRepository();
}

void PersonService::closeDatabase()
{
    personRepo.closeDatabase();
    computerRepo.closeDatabase();
}

void PersonService::addPerson(Person p)
{
    personRepo.addPerson(p);
}

void PersonService::addComputer(Computer c)
{
    computerRepo.addComputer(c);
}

void PersonService::addRelations(int pID, int cID)
{
    personRepo.addRelations(pID, cID);
}

vector<Person> PersonService::searchP(char sCommand, string sInp, DisplaySettings current)
{
    return personRepo.search(sCommand, sInp, current);
}

vector<Computer> PersonService::searchC(char sCommand, string sInp, DisplaySettings current)
{
    return computerRepo.search(sCommand, sInp, current);
}

void PersonService::deletePerson(Person p)
{
    personRepo.deletePerson(p);
}

void PersonService::deleteComputer(Computer c)
{
    computerRepo.deleteComputer(c);
}

void PersonService::deleteRelations(int sID, int cID)
{
    personRepo.deleteRelations(sID, cID);
}

vector<Person> PersonService::getScientists(DisplaySettings current)
{
    return personRepo.getScientists(current);
}

vector<Computer> PersonService::getComputers(DisplaySettings current)
{
    return computerRepo.getComputers(current);
}

vector<PRelations> PersonService::getPRelations()
{
    return personRepo.getRelations();
}

vector<CRelations> PersonService::getCRelations()
{
    return computerRepo.getRelations();
}
