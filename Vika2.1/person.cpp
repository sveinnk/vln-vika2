#include "person.h"
#include <iomanip>
#include <iostream>

Person::Person()
{
    firstName = "";
    lastName = "";
    gender = "";
    yearOfBirth = '\0';
    yearOfDeath = '\0';
    personID = '\0';
}

ostream& operator << (ostream& outs, const Person& person)
{
    int personID = person.personID;
    string firstName = person.firstName;
    string lastName = person.lastName;
    string gender = person.gender;
    int yearOfBirth = person.yearOfBirth;
    int yearOfDeath = person.yearOfDeath;

    outs << setw(5) << personID << setw(15) << firstName << setw(15) << lastName << setw(12) << gender;
    outs << setw(20) << yearOfBirth << setw(20) << yearOfDeath << endl << endl;

    return outs;
}

