#ifndef CRELATIONS_H
#define CRELATIONS_H

#include <string>

using namespace std;

class CRelations
{
public:
    CRelations();
    int scientistID;
    int computerID;
    string cName;
    friend ostream& operator << (ostream& outs, const CRelations& relations);
};

#endif // CRELATIONS_H
