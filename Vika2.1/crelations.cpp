#include "crelations.h"
#include <iomanip>
#include <iostream>


CRelations::CRelations()
{
    scientistID = '\0';
    computerID = '\0';
    cName = "";
}

ostream& operator << (ostream& outs, const CRelations& prelations)
{
    int sID = prelations.scientistID;
    string name = prelations.cName;
    int cID = prelations.computerID;

    outs << setw(15) << cID << setw(35) << name << setw(20) << sID << endl;

    return outs;
}
