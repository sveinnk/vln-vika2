#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "computer.h"
#include "person.h"
#include "prelations.h"
#include "crelations.h"
#include "personrepository.h"
#include "computerrepository.h"
#include "displaysettings.h"

class PersonService
{
private:
    PersonRepository personRepo;
    ComputerRepository computerRepo;
public:
    PersonService();
    void openDatabase();
    void closeDatabase();
    void addPerson(Person p);
    void addComputer(Computer c);
    void addRelations(int pID, int cID);
    void deletePerson(Person p);
    void deleteComputer(Computer c);
    void deleteRelations(int sID, int cID);
    vector<Person> getScientists(DisplaySettings current);
    vector<Computer> getComputers(DisplaySettings current);
    vector<PRelations> getPRelations();
    vector<CRelations> getCRelations();
    vector<Person> searchP(char sCommand, string sInp, DisplaySettings current);
    vector<Computer> searchC(char sCommand, string sInp, DisplaySettings current);
};

#endif // PERSONSERVICE_H
