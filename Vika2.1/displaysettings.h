
#ifndef DISPLAYSETTINGS_H
#define DISPLAYSETTINGS_H
#include <string>

using namespace std;


class DisplaySettings
{
public:
    DisplaySettings();
    char sortPByChar;
    bool sortPTypeBool;
    char sortCByChar;
    bool sortCTypeBool;
    bool relationsSet;
    void setRSort();
    void setPSort();
    void setCSort();
    string relationsString;
    string filterP;
    string filterC;
    string sortPBy;
    string sortPBySQL;
    string sortPType;
    string sortPTypeSQL;
    string sortCBy;
    string sortCBySQL;
    string sortCType;
    string sortCTypeSQL;
};

#endif // DISPLAYSETTINGS_H
