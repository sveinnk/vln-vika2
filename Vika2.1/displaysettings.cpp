#include "displaysettings.h"


DisplaySettings::DisplaySettings()
{
    sortPByChar = 'I';
    sortPTypeBool = true;
    sortCByChar = 'I';
    sortCTypeBool = true;
    relationsSet = true;

    setPSort();
    setCSort();
    setRSort();
}

void DisplaySettings::setPSort()
{
    switch(sortPByChar){
        case 'I':
            sortPBy = "ID";
            sortPBySQL = "ID";
            if(sortPTypeBool){
                sortPType = "Ascending order.";
                sortPTypeSQL = "ASC";
            }else{
                sortPType = "Descending order.";
                sortPTypeSQL = "DESC";
            }
            break;
        case 'F':
            sortPBy = "First Name";
            sortPBySQL = "FirstName";
            if(sortPTypeBool){
                sortPType = "Alphabetical order.";
                sortPTypeSQL = "ASC";
            }else{
                sortPType = "Reversed alphabetical order.";
                sortPTypeSQL = "DESC";
            }
            break;
        case 'L':
            sortPBy = "Last Name";
            sortPBySQL = "LastName";
            if(sortPTypeBool){
                sortPType = "Alphabetical order.";
                sortPTypeSQL = "ASC";
            }else{
                sortPType = "Reversed alphabetical order.";
                sortPTypeSQL = "DESC";
            }
            break;
        case 'G':
            sortPBy = "Gender";
            sortPBySQL = "Gender";
            if(sortPTypeBool){
                sortPType = "Males first.";
                sortPTypeSQL = "ASC";
            }else{
                sortPType = "Reversed alphabetical order.";
                sortPTypeSQL = "Desc";
            }
            break;
        case 'B':
            sortPBy = "Year Of Birth";
            sortPBySQL = "YearOfBirth";
            if(sortPTypeBool){
                sortPType = "Ascending order.";
                sortPTypeSQL = "ASC";
            }else{
                sortPType = "Descending order.";
                sortPTypeSQL = "DESC";
            }
            break;
        case 'D':
            sortPBy = "Year Of Death";
            sortPBySQL = "YearOfDeath";
            if(sortPTypeBool){
                sortPType = "Ascending order.";
                sortPTypeSQL = "ASC";
            }else{
                sortPType = "Descending order.";
                sortPTypeSQL = "DESC";
            }
            break;
    }

}

void DisplaySettings::setCSort()
{
    switch(sortCByChar){
        case 'I':
            sortCBy = "ID";
            sortCBySQL = "ID";
            if(sortCTypeBool){
                sortCType = "Ascending order.";
                sortCTypeSQL = "ASC";
            }else{
                sortCType = "Descending order.";
                sortCTypeSQL = "DESC";
            }
            break;
        case 'N':
            sortCBy = "Name";
            sortCBySQL = "Name";
            if(sortCTypeBool){
                sortCType = "Alphabetical order.";
                sortCTypeSQL = "ASC";
            }else{
                sortCType = "Reversed alphabetical order.";
                sortCTypeSQL = "DESC";
            }
            break;
        case 'D':
            sortCBy = "Year of Discovery";
            sortCBySQL = "Discovery";
            if(sortCTypeBool){
                sortCType = "Ascending order.";
                sortCTypeSQL = "ASC";
            }else{
                sortCType = "Descending order.";
                sortCTypeSQL = "DESC";
            }
            break;
        case 'T':
            sortCBy = "Computer Type";
            sortCBySQL = "Type";
            if(sortCTypeBool){
                sortCType = "Alpabetical order";
                sortCTypeSQL = "ASC";
            }else{
                sortCType = "Reversed alphabetical order.";
                sortCTypeSQL = "DESC";
            }
            break;
        case 'C':
            sortCBy = "Creation Status";
            sortCBySQL = "Built";
            if(sortCTypeBool){
                sortCType = "Built first";
                sortCTypeSQL = "ASC";
            }else{
                sortCType = "Not built first.";
                sortCTypeSQL = "DESC";
            }
            break;
    }
}

void DisplaySettings::setRSort()
{
    if(relationsSet){
        relationsString = "Scientists.";
    }else{

        relationsString = "Computers.";
    }
}
