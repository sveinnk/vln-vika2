#include "prelations.h"
#include <iomanip>
#include <iostream>

PRelations::PRelations()
{
    scientistID = '\0';
    computerID = '\0';
    pFirstName = "";
    pLastName = "";
}

ostream& operator << (ostream& outs, const PRelations& prelations)
{
    int sID = prelations.scientistID;
    string fName = prelations.pFirstName;
    string lName = prelations.pLastName;
    int cID = prelations.computerID;

    outs << setw(15) << sID << setw(15) << fName << setw(15) << lName << setw(15) << cID << endl;

    return outs;
}
