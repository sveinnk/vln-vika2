#-------------------------------------------------
#
# Project created by QtCreator 2014-12-05T15:33:55
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = Vika2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    displaysettings.cpp \
    person.cpp \
    personrepository.cpp \
    personservice.cpp \
    computer.cpp \
    utility.cpp \
    computerrepository.cpp \
    prelations.cpp \
    crelations.cpp

HEADERS += \
    consoleui.h \
    displaysettings.h \
    person.h \
    personrepository.h \
    personservice.h \
    computer.h \
    utility.h \
    computerrepository.h \
    prelations.h \
    crelations.h
