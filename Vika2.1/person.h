#ifndef PERSON_H
#define PERSON_H

#include <string>

using namespace std;

class Person
{
public:
    Person();
    string firstName;
    string lastName;
    string gender;
    int yearOfBirth;
    int yearOfDeath;
    int personID;
    friend ostream& operator << (ostream& outs, const Person& person);

};

#endif // PERSON_H
