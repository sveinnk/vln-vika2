#include "computer.h"
#include <iomanip>
#include <iostream>

Computer::Computer()
{
    computerID = '\0';
    name = "";
    discovery = '\0';
    type = "";
    built = "";
}

ostream& operator << (ostream& outs, const Computer& computer)
{
    int computerID = computer.computerID;
    string name = computer.name;
    int discovery = computer.discovery;
    string type = computer.type;
    string built = computer.built;

    outs << setw(5) << computerID << setw(35) << name << setw(20) << discovery << setw(15) << type << setw(10) << built << endl << endl;

    return outs;
}
