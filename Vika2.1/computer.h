#ifndef COMPUTER_H
#define COMPUTER_H
#include <string>

using namespace std;


class Computer
{
public:
    Computer();
    int computerID;
    string name;
    int discovery;
    string type;
    string built;
    friend ostream& operator << (ostream& outs, const Computer& computer);
};

#endif // COMPUTER_H
