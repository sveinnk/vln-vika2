#include "computerrepository.h"
#include "utility.h"
#include <iostream>
#include "crelations.h"
#include <fstream>
#include <algorithm>
#include <cctype>

const char* FILE_PATH2 = "..//Vika2.1//gagnagrunnur.sqlite";

ComputerRepository::ComputerRepository()
{
    QSqlDatabase db;
    openDatabase();
}

void ComputerRepository::openDatabase()
{
    QString connectionName = "ComputerConnection";

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName(FILE_PATH2);
        db.open();
    }
}

void ComputerRepository::closeDatabase()
{
    db.close();
}

vector<Computer> ComputerRepository::getComputers(DisplaySettings current)
{
    Computer c;
    vector<Computer> computerList;

    QString queryString = "SELECT * FROM Computers WHERE Deleted = 0 ORDER BY " + QString::fromStdString(current.sortCBySQL) + " " + QString::fromStdString(current.sortCTypeSQL);

    QSqlQuery query(db);
    query.prepare(queryString);
    query.exec();

    while(query.next())
    {
        c.computerID = query.value("ID").toInt();
        c.name = query.value("Name").toString().toStdString();
        c.discovery = query.value("Discovery").toInt();
        c.type = query.value("Type").toString().toStdString();
        c.built = query.value("Built").toString().toStdString();
        computerList.push_back(c);
    }

    return computerList;
}

void ComputerRepository::addComputer(Computer c)
{
    QSqlQuery query(db);
    query.prepare("INSERT INTO Computers (Name, Discovery, Type, Built)"
                  "VALUES (:name, :discovery, :type, :built);");
    query.bindValue(":name", QVariant(QString::fromStdString(c.name)));
    query.bindValue(":discovery", c.discovery);
    query.bindValue(":type", QVariant(QString::fromStdString(c.type)));
    query.bindValue(":built", QVariant(QString::fromStdString(c.built)));
    query.exec();
}

vector<Computer> ComputerRepository::search(char sCommand, string sInp, DisplaySettings current)
{
    Computer c;
    string column;
    vector<Computer> searchList;

    sInp = toLowercase(sInp);
    sInp[0] = toupper(sInp[0]);

    switch(sCommand){
        case 'I':
            column = "ID";
            break;
        case 'N':
            column = "Name";
            break;
        case 'D':
            column = "Discovery";
            break;
        case 'T':
            column = "Type";
            break;
        case 'B':
            column = "Built";
            break;
        default:
            break;
    }

    QSqlQuery query(db);

    QString queryString = "SELECT * FROM Computers WHERE Deleted = 0 AND " + QString::fromStdString(column) + " = " + "'" + QString::fromStdString(sInp) + "'" + " ORDER BY " + QString::fromStdString(current.sortCBySQL) + " " + QString::fromStdString(current.sortCTypeSQL);
    query.prepare(queryString);
    query.exec();

    while(query.next())
    {
        c.computerID = query.value("ID").toInt();
        c.name = query.value("Name").toString().toStdString();
        c.type = query.value("Type").toString().toStdString();
        c.discovery = query.value("Discovery").toInt();
        c.built = query.value("Built").toString().toStdString();
        searchList.push_back(c);
    }

    return searchList;

}

void ComputerRepository::deleteComputer(Computer c)
{
    QSqlQuery query(db);

    query.prepare("UPDATE Computers SET Deleted = 1 WHERE ID = :cID;");
    query.bindValue(":cID", c.computerID);
    query.exec();
}

vector<CRelations> ComputerRepository::getRelations()
{
    vector<CRelations> relationlist;
    CRelations r;
    QSqlQuery query(db);
    QSqlQuery pquery(db);
    QSqlQuery cquery(db);
    int sID, cID;
    query.exec("SELECT * FROM Relations WHERE Deleted = 0;");
    while(query.next())
    {
        sID = query.value("ScientistID").toInt();
        pquery.prepare("SELECT * FROM Scientists WHERE ID = :sid AND Deleted = 0;");
        pquery.bindValue(":sid", sID);
        pquery.exec();
        while(pquery.next())
        {
            r.scientistID = pquery.value("ID").toInt();
        }
        cID = query.value("ComputerID").toInt();
        cquery.prepare("SELECT * FROM Computers WHERE ID = :cid AND Deleted = 0;");
        cquery.bindValue(":cid", cID);
        cquery.exec();
        while(cquery.next())
        {
            r.cName = cquery.value("Name").toString().toStdString();
            r.computerID = cquery.value("ID").toInt();
        }
        relationlist.push_back(r);
    }
    return relationlist;
}

