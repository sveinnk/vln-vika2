#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include <vector>
#include <QtSql>
#include "person.h"
#include "prelations.h"
#include "utility.h"
#include "displaysettings.h"

using namespace std;

class PersonRepository
{
private:
    vector<Person> personList;
    QSqlDatabase db;
public:
    PersonRepository();  
    void addPerson(Person p);
    void addRelations(int pID, int cID);
    void deletePerson(Person p);
    void deleteRelations(int sID, int cID);
    vector<Person> getScientists(DisplaySettings current);
    vector<PRelations> getRelations();
    void openDatabase();
    void closeDatabase();
    vector<Person> search(char sCommand, string sInp, DisplaySettings current);
};

#endif // PERSONREPOSITORY_H
