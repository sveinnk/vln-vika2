#ifndef PRELATIONS_H
#define PRELATIONS_H

#include <string>

using namespace std;

class PRelations
{
public:
    PRelations();
    int scientistID;
    int computerID;
    string pFirstName;
    string pLastName;
    friend ostream& operator << (ostream& outs, const PRelations& relations);
};

#endif // PRELATIONS_H
