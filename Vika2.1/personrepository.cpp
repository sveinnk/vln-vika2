#include "personrepository.h"
#include "utility.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cctype>
#include <sstream>
#include <string>

const char* FILE_PATH = "..//Vika2.1//gagnagrunnur.sqlite";

using namespace std;


PersonRepository::PersonRepository()
{
    QSqlDatabase db;
    openDatabase();
}

void PersonRepository::openDatabase()
{
    QString connectionName = "ScientistConnection";

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName(FILE_PATH);
        db.open();
    }
}

void PersonRepository::closeDatabase()
{
    db.close();
}

vector<PRelations> PersonRepository::getRelations()
{
    vector<PRelations> relationlist;
    PRelations r;
    QSqlQuery query(db);
    QSqlQuery pquery(db);
    QSqlQuery cquery(db);
    int sID, cID;
    query.exec("SELECT * FROM Relations WHERE Deleted = 0");
    while(query.next())
    {
        sID = query.value("ScientistID").toInt();
        pquery.prepare("SELECT * FROM Scientists WHERE ID = :sid AND Deleted = 0;");
        pquery.bindValue(":sid", sID);
        pquery.exec();
        while(pquery.next())
        {
            r.pFirstName = pquery.value("FirstName").toString().toStdString();
            r.pLastName = pquery.value("LastName").toString().toStdString();
            r.scientistID = pquery.value("ID").toInt();
        }
        cID = query.value("ComputerID").toInt();
        cquery.prepare("SELECT * FROM Computers WHERE ID=:cid AND Deleted = 0;");
        cquery.bindValue(":cid", cID);
        cquery.exec();
        while(cquery.next())
        {
            r.computerID = cquery.value("ID").toInt();
        }
        relationlist.push_back(r);
    }
    return relationlist;
}

vector<Person> PersonRepository::getScientists(DisplaySettings current)
{
    Person p;
    vector<Person> scientistList;

    QString queryString = "SELECT * FROM Scientists WHERE Deleted = 0 ORDER BY " + QString::fromStdString(current.sortPBySQL) + " " + QString::fromStdString(current.sortPTypeSQL);

    QSqlQuery query(db);
    query.prepare(queryString);
    query.exec();

    while(query.next())
    {
        p.personID = query.value("ID").toInt();
        p.firstName = query.value("FirstName").toString().toStdString();
        p.lastName = query.value("LastName").toString().toStdString();
        p.gender = query.value("Gender").toString().toStdString();
        p.yearOfBirth = query.value("YearOfBirth").toInt();
        p.yearOfDeath = query.value("YearOfDeath").toInt();
        scientistList.push_back(p);
    }

    return scientistList;
}

void PersonRepository::addPerson(Person p)
{
    QSqlQuery query(db);
    query.prepare("INSERT INTO Scientists (FirstName, LastName, Gender, YearOfBirth, YearOfDeath)"
                  "VALUES (:firstname, :lastname, :gender, :yearofbirth, :yearofdeath);");
    query.bindValue(":firstname", QVariant(QString::fromStdString(p.firstName)));
    query.bindValue(":lastname", QVariant(QString::fromStdString(p.lastName)));
    query.bindValue(":gender", QVariant(QString::fromStdString(p.gender)));
    query.bindValue(":yearofbirth", p.yearOfBirth);
    query.bindValue(":yearofdeath", p.yearOfDeath);
    query.exec();

}

void PersonRepository::deletePerson(Person p)
{
    QSqlQuery query(db);

    query.prepare("UPDATE Scientists SET Deleted = 1 WHERE ID = :pID;");
    query.bindValue(":pID", p.personID);
    query.exec();
}

void PersonRepository::deleteRelations(int sID, int cID)
{
    QSqlQuery query(db);

    int validSID, validCID;

    query.prepare("SELECT * FROM Relations WHERE ScientistID = :sID");
    query.bindValue(":sID", sID);
    query.exec();
    while(query.next()){
        validSID = query.value("ScientistID").toInt();
    }

    query.prepare("SELECT * FROM Relations WHERE ComputerID = :cID");
    query.bindValue(":cID", cID);
    query.exec();
    while(query.next()){
        validCID = query.value("ComputerID").toInt();
    }

    query.prepare("UPDATE Relations SET Deleted = 1 WHERE ScientistID = :validS AND ComputerID = :validC;");
    query.bindValue(":validS", validSID);
    query.bindValue(":validC", validCID);
    query.exec();

}

void PersonRepository::addRelations(int pID, int cID)
{
    QSqlQuery query(db);

    int validPID, validCID;

    query.prepare("SELECT * FROM Scientists WHERE ID = :pID");
    query.bindValue(":pID", pID);
    query.exec();
    while(query.next()){
        validPID = query.value("ID").toInt();
    }

    query.prepare("SELECT * FROM Computers WHERE ID = :cID");
    query.bindValue(":cID", cID);
    query.exec();
    while(query.next()){
        validCID = query.value("ID").toInt();
    }

    query.prepare("INSERT INTO Relations (ScientistID, ComputerID)"
                  "VALUES (:validP, :validC);");
    query.bindValue(":validP", validPID);
    query.bindValue(":validC", validCID);
    query.exec();

}

vector<Person> PersonRepository::search(char sCommand, string sInp, DisplaySettings current)
{
    Person p;
    string column;
    vector<Person> searchList;

    sInp = toLowercase(sInp);
    sInp[0] = toupper(sInp[0]);

    switch(sCommand){
        case 'I':
            column = "ID";
            break;
        case 'F':
            column = "FirstName";
            break;
        case 'L':
            column = "LastName";
            break;
        case 'G':
            column = "Gender";
            break;
        case 'B':
            column = "YearOfBirth";
            break;
        case 'D':
            column = "YearOfDeath";
            break;
        default:
            break;
    }

    QSqlQuery query(db);

    QString queryString = "SELECT * FROM Scientists WHERE Deleted = 0 AND " + QString::fromStdString(column) + " = " + "'" + QString::fromStdString(sInp) + "'" + " ORDER BY " + QString::fromStdString(current.sortPBySQL) + " " + QString::fromStdString(current.sortPTypeSQL);
    query.prepare(queryString);
    query.exec();


    while(query.next())
    {
        p.personID = query.value("ID").toInt();
        p.firstName = query.value("FirstName").toString().toStdString();
        p.lastName = query.value("LastName").toString().toStdString();
        p.gender = query.value("Gender").toString().toStdString();
        p.yearOfBirth = query.value("YearOfBirth").toInt();
        p.yearOfDeath = query.value("YearOfDeath").toInt();
        searchList.push_back(p);
    }

    return searchList;
}
