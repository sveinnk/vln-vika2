#ifndef COMPUTERREPOSITORY_H
#define COMPUTERREPOSITORY_H

#include "crelations.h"
#include <displaysettings.h>
#include <vector>
#include <QtSql>
#include "computer.h"
#include "utility.h"
#include "displaysettings.h"

using namespace std;

class ComputerRepository
{
private:
    vector<Computer> computerList;
    QSqlDatabase db;
public:
    ComputerRepository();
    vector<Computer> search(char sCommand, string sInp, DisplaySettings current);
    void addComputer(Computer c);
    void deleteComputer(Computer c);
    void openDatabase();
    void closeDatabase();
    vector<Computer> getComputers(DisplaySettings current);
    vector<CRelations> getRelations();
};

#endif // COMPUTERREPOSITORY_H
