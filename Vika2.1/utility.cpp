#include "utility.h"

string toUppercase(string input)
{
    for(unsigned int i = 0; i < input.length(); i++){
        input[i] = toupper(input[i]);
    }

    return input;
}

string toLowercase(string input)
{
    for(unsigned int i = 0; i < input.length(); i++){
        input[i] = tolower(input[i]);
    }

    return input;
}

