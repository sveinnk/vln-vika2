#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <string>
#include <iostream>
#include "personservice.h"
#include "displaysettings.h"

class ConsoleUI
{
public:
    ConsoleUI();
    void start();
private:
    PersonService personService;
    void addPerson();
    void addComputer();
    void addRelations(DisplaySettings current);
    void searchPerson(char &sCommand, string &sInp);
    void searchComputer(char &sCommand, string &sInp);
    void deletePerson(DisplaySettings current);
    void deleteComputer(DisplaySettings current);
    void deleteRelations(DisplaySettings current);
    void lines();
    void displayPerson(vector<Person> personlist);
    void displayComputer(vector<Computer> computerlist);
    void displayPRelations(vector<PRelations> relationlist);
    void displayCRelations(vector<CRelations> relationlist);
    void displaysettings(DisplaySettings& current);
    void scientistSorting(DisplaySettings& current);
    void computerSorting(DisplaySettings& current);
    void relationsSorting(DisplaySettings& current);
    void seeCommands();
    void welcomeLine();
    char displayChoice();
    bool check2options(char input, char option1, char option2);
    bool check3options(char input, char option1, char option2, char option3);
    bool check4options(char input, char option1, char option2, char option3, char option4);
    bool check5options(char input, char option1, char option2, char option3, char option4, char option5);
    bool check6options(char input, char option1, char option2, char option3, char option4, char option5, char option6);
    char addChoice();
    bool searchChoice();
    char deleteChoice();
};

#endif // CONSOLEUI_H
